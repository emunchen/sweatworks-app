import React, { Component } from 'react';
import Header from './header';
import PostList from './postList';
import AuthorList from './authorList';

class HomePage extends Component {
  render() {
    return (
      <div>
        <Header
          title="Welcome to Fitness News"
          subtitle="Expert advice, analysis and insight to help you lead a physically
        fitter and mentally happier life."
        />
        <section>
          <div className="container">
            <div className="row">
              <div className="col-lg-8">
                <PostList />
              </div>
              <div className="col-lg-4">
                <AuthorList />
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default HomePage;
