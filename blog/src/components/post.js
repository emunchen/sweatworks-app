import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/post.css';

class Post extends Component {
  render() {
    return (
      <div className="media">
        <div className="media-left">
          <Link key={this.props.id} to={'/posts/' + this.props.id}>
            <img
              width="150px"
              className="media-object"
              src="/images/products/image-yellow.png"
              alt={this.props.title}
            />
          </Link>
        </div>
        <div className="media-body">
          <Link key={this.props.id} to={'/posts/' + this.props.id}>
            <h4 className="media-heading">{this.props.title}</h4>{' '}
          </Link>
          <small>
            {this.props.authorName} ({this.props.authorEmail})
          </small>
          <p>{this.props.content.slice(0, 280)}</p>
        </div>
      </div>
    );
  }
}

export default Post;
