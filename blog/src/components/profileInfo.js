import React, { Component } from 'react';

class ProfileInfo extends Component {
  render() {
    return (
      <section>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="span3 well">
                <center>
                  <a
                    href="#aboutModal"
                    data-toggle="modal"
                    data-target="#myModal"
                  >
                    <img
                      src={this.props.avatar_url}
                      name="aboutme"
                      alt="..."
                      width="140"
                      height="140"
                      className="img-circle"
                    />
                  </a>
                  <h3>{this.props.name}</h3>
                  <h4>{this.props.email}</h4>
                  <em>{this.props.birth}</em>
                </center>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default ProfileInfo;
