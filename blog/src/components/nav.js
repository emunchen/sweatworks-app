import React, { Component } from 'react';
import { Navbar, Grid } from 'react-bootstrap';
import SearchBox from './searchBox';

class Nav extends Component {
  render() {
    return (
      <Navbar inverse fixedTop>
        <Grid>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="/">Home</a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <SearchBox />
        </Grid>
      </Navbar>
    );
  }
}

export default Nav;
