import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/resultBox.css';

class ResultBox extends Component {
  render() {
    const postsItems = this.props.result.map(post => (
      <Link key={post.id} to={'/posts/' + post.id}>
        <li key={post.id}>{post.postTitle}</li>
      </Link>
    ));
    return <ul className="result-list">{postsItems}</ul>;
  }
}

export default ResultBox;
