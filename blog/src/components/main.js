import React, { Component } from 'react';
import HomePage from './homePage';
import ProfilePage from './profilePage';
import PostPage from './postPage';
import { Switch, Route } from 'react-router-dom';
import '../css/main.css';

class Main extends Component {
  render() {
    return (
      <div className="main-container">
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/profile/:id" component={ProfilePage} />
          <Route path="/posts/:id" component={PostPage} />
        </Switch>
      </div>
    );
  }
}

export default Main;
