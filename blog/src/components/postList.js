import React, { Component } from 'react';
import Post from './post';
import { client } from '../client';

class PostList extends Component {
  state = {
    posts: []
  };

  componentDidMount() {
    this.loadPostsFromServer();
  }

  loadPostsFromServer = () => {
    client.getPosts(serverPosts => {
      this.setState({ posts: serverPosts.result });
    });
  };

  render() {
    const postComponents = this.state.posts.map(post => (
      <Post
        key={'post-' + post.id}
        id={post.id}
        title={post.postTitle}
        content={post.postContent}
        created={post.createdAt}
        authorName={post.Author.name}
        authorEmail={post.Author.email}
      />
    ));
    return (
      <div>
        <section>
          <h2>Latest Posts</h2>
          {postComponents}
        </section>
      </div>
    );
  }
}

export default PostList;
