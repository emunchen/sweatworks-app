import React, { Component } from 'react';
import Post from './post';
import Nav from './nav';
import ProfileInfo from './profileInfo';
import { client } from '../client';
import '../css/profilePage.css';

class ProfilePage extends Component {
  state = {
    profile: {},
    authorPosts: []
  };
  componentDidMount() {
    this.loadAuthorPostsFromServer(this.props.match.params.id);
    this.loadAuthorFromServer(this.props.match.params.id);
  }
  loadAuthorFromServer = authorId => {
    client.getAuthor(authorId).then(author => {
      this.setState({ profile: author });
    });
  };
  loadAuthorPostsFromServer = authorId => {
    client.getAuthorPosts(authorId).then(serverAuthorPosts => {
      this.setState({ authorPosts: serverAuthorPosts });
    });
  };

  render() {
    const authorProfile = this.state.profile;
    const authorPostsComponents = this.state.authorPosts.map(post => (
      <Post
        key={'post-' + post.id}
        id={post.id}
        title={post.postTitle}
        content={post.postContent}
        created={post.createdAt}
        authorName={post.Author.name}
        authorEmail={post.Author.email}
      />
    ));
    return (
      <div>
        <Nav />
        <section className="profile-container">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <ProfileInfo
                  name={authorProfile.name}
                  birth={authorProfile.birth}
                  email={authorProfile.email}
                  avatar_url={authorProfile.avatar_url}
                />
                {authorPostsComponents}
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default ProfilePage;
