import React, { Component } from 'react';

class PostInfo extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <h1>{this.props.postTitle}</h1>
            <p>{this.props.postContent}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default PostInfo;
