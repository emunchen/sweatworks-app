import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/author.css';

class Author extends Component {
  render() {
    return (
      <li className="list-group-item">
        <div className="media">
          <div className="media-left">
            <a href="/">
              <img
                width="70px"
                className="media-object"
                src={this.props.avatar_url}
                alt={this.props.name}
              />
            </a>
          </div>
          <div className="media-body">
            <h4 className="media-heading">{this.props.name}</h4>
            <p>{this.props.email}</p>
            <Link to={'/profile/' + this.props.id}>See All Posts</Link>
          </div>
        </div>
      </li>
    );
  }
}

export default Author;
