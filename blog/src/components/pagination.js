import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/pagination.css';

class Pagination extends Component {
  state = {
    currentPage: 1,
    postsPerPage: 4,
    posts: []
  };

  handleClick = e => {
    const value = e.target.id;
    this.setState({
      currentPage: Number(value)
    });
  };

  componentDidMount() {}

  render() {
    const posts = this.props.posts;
    const { currentPage, postsPerPage } = this.state;
    // Logic for displaying todos
    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

    const renderPosts = currentPosts.map((post, index) => {
      return <li key={index}>{post}</li>;
    });

    // Logic for displaying page numbers
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(posts.length / postsPerPage); i++) {
      pageNumbers.push(i);
    }
    const renderPageNumbers = pageNumbers.map(number => {
      return (
        <li
          number={this.props.posts}
          key={number}
          id={number}
          onClick={this.handleClick}
        >
          {number}
        </li>
      );
    });

    return (
      <div>
        <ul>{renderPosts}</ul>
        <ul className="pagination">{renderPageNumbers}</ul>
      </div>
    );
  }
}

export default Pagination;
