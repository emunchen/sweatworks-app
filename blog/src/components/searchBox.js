import React, { Component } from 'react';
import ResultBox from './resultBox';
import { client } from '../client';
import '../css/searchBox.css';

class SearchBox extends Component {
  state = {
    posts: [],
    searchValue: ''
  };
  onSearchChange = e => {
    const value = e.target.value;
    this.setState({
      searchValue: value
    });
    if (value === '') {
      this.setState({
        posts: []
      });
    } else {
      client.searchPosts(value, posts => {
        this.setState({
          posts: posts
        });
      });
    }
  };
  render() {
    return (
      <div className="search-box-container">
        <form className="navbar-form navbar-right">
          <div className="form-group">
            <input
              className="form-control"
              placeholder="search"
              value={this.state.searchValue}
              onChange={this.onSearchChange}
            />
          </div>
          <button type="submit" className="btn btn-default">
            Submit
          </button>
        </form>
        <ResultBox result={this.state.posts} />
      </div>
    );
  }
}

export default SearchBox;
