import React, { Component } from 'react';
import '../css/footer.css';
class Footer extends Component {
  render() {
    return (
      <footer>
        <div className="container">
          <p className="m-0 text-center text-white">
            Copyright © Your Website 2017
          </p>
        </div>
      </footer>
    );
  }
}

export default Footer;
