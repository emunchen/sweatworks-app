import React, { Component } from 'react';
import Nav from './nav';
import '../css/header.css';

class Header extends Component {
  render() {
    return (
      <div>
        <Nav />
        <header className="bg-primary text-white">
          <div className="container text-center">
            <h1>{this.props.title}</h1>
            <p className="lead">{this.props.subtitle || ''}</p>
          </div>
        </header>
      </div>
    );
  }
}

export default Header;
