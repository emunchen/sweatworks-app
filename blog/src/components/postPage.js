import React, { Component } from 'react';
import Header from './header';
import PostInfo from './postInfo';
import { client } from '../client';
import '../css/postPage.css';

class PostPage extends Component {
  state = {
    post: {}
  };
  componentDidMount() {
    this.loadPostFromServer(this.props.match.params.id);
  }
  loadPostFromServer = postId => {
    client.getPost(postId).then(post => {
      this.setState({ post: post });
    });
  };

  render() {
    const post = this.state.post;
    return (
      <div>
        <Header title={post.postTitle} />
        <section className="post-container">
          <div className="container">
            <div className="row">
              <div className="col-lg-6">
                <PostInfo
                  postTitle={post.postTitle}
                  postContent={post.postContent}
                  createdAt={post.createdAt}
                  updatedAt={post.updatedAt}
                />
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default PostPage;
