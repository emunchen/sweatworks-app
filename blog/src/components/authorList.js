import React, { Component } from 'react';
import Author from './author';
import { client } from '../client';

class AuthorList extends Component {
  state = {
    authors: []
  };

  componentDidMount() {
    this.loadAuthorsFromServer();
  }

  loadAuthorsFromServer = () => {
    client.getAuthors(serverAuthors =>
      this.setState({ authors: serverAuthors })
    );
  };

  render() {
    const authorComponents = this.state.authors.map(author => (
      <Author
        key={'author-' + author.id}
        id={author.id}
        name={author.name}
        email={author.email}
        avatar_url={author.avatar_url}
        created={author.createdAt}
      />
    ));
    return (
      <div>
        <h2>Best Authors</h2>
        <ul className="list-group">{authorComponents}</ul>
      </div>
    );
  }
}

export default AuthorList;
