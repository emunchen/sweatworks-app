import React, { Component } from 'react';
import Footer from './components/footer';
import Main from './components/main';

class App extends Component {
  render() {
    return (
      <div>
        <Main />
        <Footer />
      </div>
    );
  }
}

export default App;
