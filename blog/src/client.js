const client = {
  searchPosts(query, cb) {
    return fetch('/posts/_search', {
      method: 'post',
      body: JSON.stringify({ query: query }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(this.checkStatus)
      .then(this.parseJSON)
      .then(cb)
      .catch(error => console.log(error.message));
  },
  getPosts: function(success) {
    return fetch('/posts', {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(this.checkStatus)
      .then(this.parseJSON)
      .then(success);
  },
  getAuthors: function(success) {
    return fetch('/authors', {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(this.checkStatus)
      .then(this.parseJSON)
      .then(success);
  },
  getAuthorPosts: function(authorId) {
    return fetch('/authors/' + authorId + '/posts', {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(this.checkStatus)
      .then(this.parseJSON);
  },
  getAuthor: function(authorId) {
    return fetch('/authors/' + authorId, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(this.checkStatus)
      .then(this.parseJSON);
  },
  getPost: function(postId) {
    return fetch('/posts/' + postId, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(this.checkStatus)
      .then(this.parseJSON);
  },
  checkStatus: function(response) {
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      console.log(response);
    }
  },
  parseJSON: function(response) {
    return response.json();
  }
};

module.exports = { client };
