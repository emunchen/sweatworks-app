var env = process.env.NODE_ENV || 'development';

if (env === 'development') {
  process.env.PORT = 3012;
  process.env.DB_NAME = 'sweatworks';
} else if (env === 'test') {
  process.env.PORT = 3012;
  process.env.DB_NAME = 'sweatworks_test';
}
