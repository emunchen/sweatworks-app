const Sequelize = require('sequelize');

console.log('Environment: ' + process.env.DB_NAME);

const sequelize = new Sequelize(process.env.DB_NAME, null, null, {
  host: 'localhost',
  dialect: 'sqlite',
  logging: false,
  operatorsAliases: false,
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  storage: './data.sqlite'
});

module.exports = { sequelize };
