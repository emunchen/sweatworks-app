const expect = require('expect');
const request = require('supertest');
const { app } = require('./../server');
const { Post } = require('./../models/post');
const { Author } = require('./../models/author');
const seed = require('./seed.json');

Author.sync({ force: true }).then(() => {
  Author.create(seed.authors[0]).then(function(newAuthor) {
    seed.posts.map(post => {
      Post.create({
        postTitle: post.postTitle,
        postContent: post.postContent,
        AuthorId: newAuthor.id
      });
    });
  });
});

var post = {
  id: 12,
  postTitle: 'Post Title test',
  postContent: 'Post Content test',
  AuthorId: 1
};

var author = {
  id: 1,
  name: 'Ema Pecora',
  email: 'epecora@gmail.com',
  birth: '1988-12-20'
};

// AUTHORS
describe('POST /authors', function() {
  this.timeout(5000);
  it('should create a new author', function(done) {
    request(app)
      .post('/authors')
      .send({
        name: author.name,
        email: author.email,
        birth: author.birth
      })
      .expect(200)
      .expect(function(res) {
        expect(res.body).toEqual({
          name: author.name,
          email: author.email,
          birth: author.birth
        });
      })
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });

  it('should not create an author with invalid body data', function(done) {
    request(app)
      .post('/authors')
      .send({})
      .expect(400)
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });
});

describe('GET /authors', function() {
  this.timeout(5000);
  it('should get all authors', function(done) {
    request(app)
      .get('/authors')
      .expect(200)
      .expect(res => {
        expect(res.body.length).toBe(5);
      })
      .end(done);
  });
});

describe('GET /authors/:id', function() {
  it('should return a author', function(done) {
    var id = author['id'];
    request(app)
      .get(`/authors/${id}`)
      .expect(200)
      .expect(function(res) {
        expect(res.body.email).toBe(author.email);
      })
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });

  it('should return 404 if post not found', function(done) {
    var id = Math.floor(Math.random() * 9909) + 100;
    request(app)
      .get(`/authors/${id}`)
      .expect(404)
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });
});

describe('PATCH /authors/:id', function() {
  it('should update an author', function(done) {
    var id = author['id'];
    var authorEmail = 'ema.pecora@gmail.com';

    request(app)
      .patch(`/authors/${id}`)
      .send({
        email: authorEmail,
        name: author.name
      })
      .expect(204)
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });
});

describe('DELETE /authors/:id', function() {
  this.timeout(5000);
  it('should remove an author', function(done) {
    var id = 4;
    request(app)
      .delete(`/authors/${id}`)
      .expect(200)
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });

  it('should return 404 if post not found', function(done) {
    var id = Math.floor(Math.random() * 9909) + 100;
    request(app)
      .delete(`/authors/${id}`)
      .expect(404)
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });
});

// POSTS
describe('POST /posts', function() {
  it('should create a new post', function(done) {
    request(app)
      .post('/posts')
      .send({
        postTitle: post.postTitle,
        postContent: post.postContent,
        AuthorId: post.AuthorId
      })
      .expect(200)
      .expect(function(res) {
        expect(res.body).toEqual(post);
      })
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });

  it('should not create a post with invalid body data', function(done) {
    request(app)
      .post('/posts')
      .send({})
      .expect(400)
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });
});

describe('GET /posts', function() {
  this.timeout(5000);
  it('should get all posts', function(done) {
    request(app)
      .get('/posts')
      .expect(200)
      .expect(function(res) {
        expect(res.body.result.length).toBe(4);
      })
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });
});

describe('GET /posts/:id', function() {
  it('should return a post', function(done) {
    var id = post['id'];
    request(app)
      .get(`/posts/${id}`)
      .expect(200)
      .expect(res => {
        expect(res.body.postTitle).toBe(post.postTitle);
      })
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });

  it('should return 404 if post not found', function(done) {
    var id = Math.floor(Math.random() * 9909) + 100;
    request(app)
      .get(`/posts/${id}`)
      .expect(404)
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });
});

describe('PATCH /posts/:id', function() {
  it('should update the post', function(done) {
    var id = post['id'];
    var postContent = 'This should be the new text';

    request(app)
      .patch(`/posts/${id}`)
      .send({
        postContent: post.postContent,
        postTitle: post.postTitle,
        AuthorId: 1
      })
      .expect(204)
      .end(function(err, res) {
        if (err) {
          return done(err);
        }
        done();
      });
  });
});

describe('DELETE /posts/:id', () => {
  it('should remove a post', done => {
    var id = post['id'];
    request(app)
      .delete(`/posts/${id}`)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        done();
      });
  });

  it('should return 404 if post not found', done => {
    var id = Math.floor(Math.random() * 9909) + 100;
    request(app)
      .delete(`/posts/${id}`)
      .expect(404)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        done();
      });
  });
});
