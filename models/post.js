const Sequelize = require('sequelize');
const { sequelize } = require('../config/db');
const seed = require('../seed.json');

const Post = sequelize.define(
  'Post',
  {
    postTitle: {
      type: Sequelize.STRING,
      field: 'post_title',
      allowNull: false
    },
    postContent: {
      type: Sequelize.TEXT,
      field: 'post_content',
      allowNull: false
    },
    AuthorId: {
      type: Sequelize.INTEGER,
      field: 'AuthorId',
      allowNull: false
    }
  },
  {
    timestamps: true
  }
);

Post.sync({ force: true }).then(() => {
  seed.posts.map(post => {
    Post.create(post);
  });
});
module.exports = { Post };
