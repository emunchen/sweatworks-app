const Sequelize = require('sequelize');
const { sequelize } = require('../config/db');
const { Post } = require('./post');
const seed = require('../seed.json');

const Author = sequelize.define(
  'Author',
  {
    name: {
      type: Sequelize.STRING,
      field: 'author_name',
      allowNull: false
    },
    email: {
      type: Sequelize.TEXT,
      field: 'author_email',
      allowNull: false
    },
    birth: {
      type: Sequelize.DATEONLY,
      field: 'author_birth'
    },
    avatar_url: {
      type: Sequelize.TEXT,
      field: 'author_avatar_url',
      allowNull: true
    }
  },
  {
    timestamps: true
  }
);

Author.sync({ force: true }).then(() => {
  seed.authors.map(author => {
    Author.create(author);
  });
});
module.exports = { Author };
