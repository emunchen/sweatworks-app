# Fitness Blog

This project is divided in two parts: one is the server (back-end) builded with express-js (4.16.2) and the second one is the front-end builded with create-react-app under "blog" folder.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

**You'll need to have Node >= 6 on your local development machine** (but it's not required on the server)

Run the following command in the root of the project

- install all the dependencies using `npm install && cd blog && npm install`

### Installing

In the root of the project run **(Be careful not to be inside /blog folder!)**:

```
npm start
```

That's all ! Now you have to visit http://localhost:3000/


## Running the tests

To run test put the following command in the root of the project:

```
npm test
```
## API documentation

There is a file in the root of this proyect (API.postman_collection.json) where you can see all API endpoints and body structure. You can get Postman from here: [Download Postman](https://www.getpostman.com/)

## Built With

* [ExpressJS](http://expressjs.com/) - Minimalist web framework for node.
* [Create React App](https://github.com/facebook/create-react-app/) - To generate a boilerplate version of a React application.
* [Sequelize](http://docs.sequelizejs.com/) - ORM

## Contributing

Please read [CONTRIBUTING.md](https://bitbucket.org/emunchen/sweatworks-app) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

I used [GIT] (https://git-scm.com/) for versioning.

## Authors

* **Emanuel Pecora** -

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
