require('./config/config');
const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const { Author } = require('./models/author');
const { Post } = require('./models/post');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

Post.belongsTo(Author);
Author.hasMany(Post);

var app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());

app.post('/posts', (req, res) => {
  let body = _.pick(req.body, ['postTitle', 'AuthorId', 'postContent']);
  Post.sync()
    .then(function() {
      return Post.create({
        postTitle: body.postTitle,
        postContent: body.postContent,
        AuthorId: body.AuthorId
      });
    })
    .then(function(post) {
      res.send({
        postTitle: post.postTitle,
        postContent: post.postContent,
        AuthorId: post.AuthorId,
        id: post.id
      });
    })
    .catch(e => {
      res.status(400).send();
    });
});

app.post('/posts/_search', (req, res) => {
  let body = _.pick(req.body, ['query']);
  Post.findAll({
    where: {
      postTitle: { [Op.like]: '%' + body.query + '%' }
    },
    order: [['createdAt', 'DESC']],
    limit: 10
  }).then(
    function(result) {
      res.send(result);
    },
    function(err) {
      console.log(err);
    }
  );
});

app.get('/posts', (req, res) => {
  let limit = 4;
  let offset = 0;
  Post.findAndCountAll()
    .then(data => {
      let page = req.query.page || 1;
      let pages = Math.ceil(data.count / limit);
      offset = limit * (page - 1);
      Post.findAll({
        attributes: ['id', 'postTitle', 'postContent', 'AuthorId'],
        include: [
          {
            model: Author,
            where: { id: Sequelize.col('post.AuthorId') }
          }
        ],
        order: [['createdAt', 'DESC']],
        limit: limit,
        offset: offset,
        $sort: { id: 1 }
      }).then(posts => {
        res
          .status(200)
          .json({ result: posts, count: data.count, pages: pages });
      });
    })
    .catch(function(error) {
      res.status(500).send();
    });
});

app.get('/posts/:id', (req, res) => {
  let id = req.params.id;
  Post.findOne({
    where: {
      id: id
    }
  })
    .then(post => {
      if (!post) {
        return res.status(404).send();
      }
      res.send({
        postTitle: post.postTitle,
        postContent: post.postContent,
        AuthorId: post.AuthorId,
        id: post.id
      });
    })
    .catch(e => {
      res.status(400).send();
    });
});

app.delete('/posts/:id', (req, res) => {
  let id = req.params.id;
  Post.destroy({
    where: {
      id: id
    }
  }).then(post => {
    if (!post) {
      return res.status(404).send();
    }
    res.send({ id });
  });
});

app.patch('/posts/:id', (req, res) => {
  let id = req.params.id;
  let body = _.pick(req.body, ['postTitle', 'AuthorId', 'postContent']);
  Post.sync()
    .then(function() {
      return Post.update(
        {
          postTitle: body.postTitle,
          postContent: body.postContent,
          AuthorId: body.AuthorId
        },
        {
          where: {
            id: id
          }
        }
      );
    })
    .then(function(post) {
      res.status(204).send();
    })
    .catch(e => {
      res.status(400).send();
    });
});

app.post('/authors', (req, res) => {
  let body = _.pick(req.body, ['name', 'email', 'birth', 'avatar_url']);
  Author.sync()
    .then(function() {
      return Author.create({
        name: body.name,
        email: body.email,
        birth: body.birth,
        avatar_url: body.avatar_url
      });
    })
    .then(function(author) {
      res.send({
        name: author.name,
        email: author.email,
        birth: author.birth
      });
    })
    .catch(e => {
      res.status(400).send();
    });
});

app.get('/authors', (req, res) => {
  Author.findAll({
    order: [['createdAt', 'DESC']],
    limit: 10
  }).then(
    function(result) {
      res.send(result);
    },
    function(err) {
      console.log(err);
    }
  );
});

app.get('/authors/:id', (req, res) => {
  let id = req.params.id;
  Author.findOne({
    where: {
      id: id
    }
  })
    .then(author => {
      if (!author) {
        return res.status(404).send();
      }
      res.send({
        id: author.id,
        name: author.name,
        email: author.email,
        birth: author.birth,
        avatar_url: author.avatar_url
      });
    })
    .catch(e => {
      res.status(400).send();
    });
});

app.get('/authors/:id/posts', (req, res) => {
  let id = req.params.id;
  Post.findAll({
    include: [
      {
        model: Author,
        where: { id: Sequelize.col('post.AuthorId') }
      }
    ],
    where: {
      authorId: id
    }
  }).then(
    function(result) {
      res.send(result);
    },
    function(err) {
      console.log(err);
    }
  );
});

app.delete('/authors/:id', (req, res) => {
  let id = req.params.id;
  Author.destroy({
    where: {
      id: id
    }
  }).then(author => {
    if (!author) {
      return res.status(404).send();
    }
    res.send({ author });
  });
});

app.patch('/authors/:id', (req, res) => {
  let id = req.params.id;
  let body = _.pick(req.body, ['name', 'email', 'birth']);
  Author.sync()
    .then(function() {
      return Author.update(
        {
          name: body.name,
          email: body.email,
          birth: body.birth
        },
        {
          where: {
            id: id
          }
        }
      );
    })
    .then(function(author) {
      res.status(204).send();
    })
    .catch(e => {
      res.status(400).send();
    });
});

app.listen(port, () => {
  console.log(`Started up at port ${port}`);
});

module.exports = { app };
